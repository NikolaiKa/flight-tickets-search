# FlightDeveloperTest ![alt text](https://www.momondo.dk/res/images/horizon/common/core/brands/momondo/favicon.ico)

This project was created with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Install dependencies
Run `npm install` for installing project's dependencies

## Run the app

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Run unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
