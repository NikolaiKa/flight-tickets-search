import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components ------------------------------------------------------------------
import { MomondoLogoComponent } from './components/momondo-logo/momondo-logo.component';

const sharedComponents = [MomondoLogoComponent];

@NgModule({
  declarations: [...sharedComponents],
  exports: [...sharedComponents],
  imports: [CommonModule]
})
export class SharedModule { }
