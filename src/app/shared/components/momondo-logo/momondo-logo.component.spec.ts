import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MomondoLogoComponent } from './momondo-logo.component';

describe('MomondoLogoComponent', () => {
  let component: MomondoLogoComponent;
  let fixture: ComponentFixture<MomondoLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MomondoLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MomondoLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });
});
