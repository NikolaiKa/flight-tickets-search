import { Component} from '@angular/core';

@Component({
  selector: 'app-momondo-logo',
  templateUrl: './momondo-logo.component.html',
  styleUrls: ['./momondo-logo.component.css']
})
export class MomondoLogoComponent {}
