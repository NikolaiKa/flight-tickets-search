import { MinutesAsHoursPipe } from './minutes-as-hours.pipe';

describe('MinutesAsHoursPipe', () => {
  let pipe: MinutesAsHoursPipe;
  const hourSymbolMock = 'h';
  const minuteSymbol = 'm';

  beforeEach(() => {
    pipe = new MinutesAsHoursPipe();
  });

  it('Should create pipe', () => {
    expect(pipe).toBeTruthy();
  });

  it('Should format duration into hours and minutes', () => {
    const durationInMinutes = 75;
    const formattedDuration = pipe.transform(durationInMinutes);

    expect(formattedDuration).toBe(`1${hourSymbolMock} 15${minuteSymbol}`);
  });

  it('Should show 0 hours + minutes if duration is less than 60 minutes', () => {
    const durationInMinutes = 59;
    const formattedDuration = pipe.transform(durationInMinutes);

    expect(formattedDuration).toBe(`0${hourSymbolMock} 59${minuteSymbol}`);
  });

  it('Should display "00" minutes when there is no reminding minutes', () => {
    const durationInMinutes = 120;
    const formattedDuration = pipe.transform(durationInMinutes);

    expect(formattedDuration).toBe(`2${hourSymbolMock} 00${minuteSymbol}`);
  });

  // formatMinutes()
  it('"formatMinutes()" Should return "00m" when minutes are 0', () => {
    const minutes = 0;
    const formattedMinutes = pipe['formatMinutes'](minutes);

    expect(formattedMinutes).toBe(`00${minuteSymbol}`);
  });

  // formatMinutes()
  it('"formatMinutes()" should prepend minutes with "0" in minutes value is less than 10', () => {
    const minutes = 9;
    const formattedMinutes = pipe['formatMinutes'](minutes);

    expect(formattedMinutes).toBe(`09${minuteSymbol}`);
  });

  // formatHours()
  it('Should return "0h" then 0 hours', () => {
    const hours = 0;
    const formattedHours = pipe['formatHours'](hours);

    expect(formattedHours).toBe(`0${hourSymbolMock}`);
  });
});
