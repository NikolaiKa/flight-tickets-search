import { Pipe, PipeTransform } from '@angular/core';
/*
  Formats minutes into hours and minutes
  Example: 75m => 1h 12m
  Accepts: duration in minutes as number
  Outputs: formatted duration as a string
*/
@Pipe({name: 'minutesAsHours'})
export class MinutesAsHoursPipe implements PipeTransform {

  // Improvement: h and m symbols should be localized
  private minuteSymbol  = 'm';
  private hourSymbol  = 'h';

  transform(durationInMinutes: number): any {
    if (!durationInMinutes) { return null; }

    if (durationInMinutes < 60) {
      return `${this.formatHours(0)} ${this.formatMinutes(durationInMinutes)}`;
    }

    const hours = Math.floor(durationInMinutes / 60);
    const minutes = durationInMinutes % 60;

    return `${this.formatHours(hours)} ${this.formatMinutes(minutes)}`;
  }

  private formatMinutes(minutes: number): string {
    return `${minutes}${this.minuteSymbol}`.padStart(3, '0');
  }

  private formatHours(hours: number): string {
    return `${hours}${this.hourSymbol}`.padStart(2, '0');
  }
}
