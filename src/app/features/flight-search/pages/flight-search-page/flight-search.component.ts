import { Component, OnInit, OnDestroy } from '@angular/core';
import { v4 as uuid } from 'uuid';

// Models ----------------------------------------------------------------------
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';
// Services --------------------------------------------------------------------
import { FlightSearchService } from '../../services/flight-search-service';
// Rxjs ------------------------------------------------------------------------
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.css']
})
export class FlightSearchPageComponent implements OnInit, OnDestroy {
  public flightTickets: FlightTicketInterface[];
  private destroyStream$ = new Subject();

  constructor(private flightSearchService: FlightSearchService) {}

  public ngOnInit(): void {
    this.listenToFlightTicketsSearchResults();
  }

  public ngOnDestroy(): void {
    this.destroyStream$.next();
    this.destroyStream$.complete();
  }

  public searchFlights(): void {
    const searchId = uuid();
    this.flightSearchService.searchFlights(searchId);
  }

  public bookButtonClickHandler(ticket: FlightTicketInterface): void {
    window.open(ticket.Deeplink, '_blank');
  }

  private listenToFlightTicketsSearchResults(): void {
    this.flightSearchService.getFlightTickets()
      .pipe(takeUntil(this.destroyStream$))
      .subscribe(flightTickets => {
        this.flightTickets = flightTickets;
      });
  }
}
