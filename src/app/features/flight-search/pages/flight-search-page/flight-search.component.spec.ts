import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

// Rxjs ------------------------------------------------------------------------
import { Observable } from 'rxjs';
// Components ------------------------------------------------------------------
import { FlightSearchPageComponent } from './flight-search.component';
// Services --------------------------------------------------------------------
import { FlightSearchService } from '../../services/flight-search-service';
import { TestingHelperService } from '../../services/testing-helper-service';
// Models ++--------------------------------------------------------------------
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';

describe('FlightSearchPageComponent', () => {
  let component: FlightSearchPageComponent;
  let fixture: ComponentFixture<FlightSearchPageComponent>;
  let flightSearchServiceMock: FlightSearchService;
  let tester: TestingHelperService;

  beforeEach(async(() => {

    setupMocks();

    TestBed.configureTestingModule({
      declarations: [ FlightSearchPageComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: FlightSearchService, useValue: flightSearchServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightSearchPageComponent);
    tester = new TestingHelperService();
    tester.setFixture(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('"Search" button should call "searchFlights()" function of the component', () => {
    const searchFlightsSpy = spyOn(component, 'searchFlights').and.callFake(() => {});

    const searchButtonSelector = '#flight-search-component-search-button';
    const searchButton = tester.getDebugElementById(searchButtonSelector);
    searchButton.triggerEventHandler('click', null);

    expect(searchFlightsSpy).toHaveBeenCalled();
  });

  it('Should render flight tickets', () => {
    const flightTicketsMock = getFlightTicketsMock();
    component.flightTickets = flightTicketsMock;
    fixture.detectChanges();

    const flightTicketSelector = '#flight-search-component-flight-tickets-container .flight-ticket';
    const renderedTickets = tester.getDebugElementsByClass(flightTicketSelector);

    expect(renderedTickets.length).toBe(flightTicketsMock.length);
  });

  it('Should not render flight tickets container if there are no flights tickets', () => {
    const flightTicketsContainerSelector = '#flight-search-component-flight-tickets-container';
    const flightTicketsContainer = tester.getDebugElementById(flightTicketsContainerSelector);

    expect(flightTicketsContainer).toBeNull();
  });

  // bookButtonClickHandler()
  it('Should open new window tab with the ticket deeplink as address value', () => {
    const windowOpenSpy = spyOn(window, 'open').and.callFake(() => {});

    const ticketMock = {
      Deeplink : 'http://www.momondo.com'
    } as FlightTicketInterface;

    component.bookButtonClickHandler(ticketMock);

    expect(windowOpenSpy).toHaveBeenCalledWith(ticketMock.Deeplink, '_blank');
  });

  // Helpers -------------------------------------------------------------------
  const setupMocks = (): void => {
    flightSearchServiceMock = {
      getFlightTickets: () => Observable.create(null)
    } as FlightSearchService;
  };

  // Helpers -------------------------------------------------------------------
  const getFlightTicketsMock = (): FlightTicketInterface[] => {
    return [{}, {}, {}] as FlightTicketInterface[];
  };
});
