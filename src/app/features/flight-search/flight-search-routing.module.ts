import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components ------------------------------------------------------------------
import { FlightSearchPageComponent } from './pages/flight-search-page/flight-search.component';

const routes: Routes = [
  {
    path: '',
    component: FlightSearchPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlightSearchRoutingModule { }
