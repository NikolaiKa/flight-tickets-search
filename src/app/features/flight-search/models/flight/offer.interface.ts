export interface OfferInterface {
  Currency: string;
  Deeplink: string;
  FlightIndex: number;
  Score: number;
  TicketClass: string;
  Price: number;
}
