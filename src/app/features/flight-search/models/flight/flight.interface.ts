export interface FlightInterface {
  Key: string;
  MaxStops: number;
  SegmentIndexes: number[];
  TicketClass: string;
}
