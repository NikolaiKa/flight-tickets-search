export interface LegInterface {
  Arrival: string | Date;
  Departure: string | Date;
  Duration: number;
  FlightNumber: number;
  Key: string;
  StopOvers: number;
  AirlineIcao: string;
  AirlineName: string;
  OriginIata: string;
  OriginDisplayName: string;
  DestinationIata: string;
  DestinationDisplayName: string;
}
