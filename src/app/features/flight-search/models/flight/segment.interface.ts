export interface SegmentInterface {
  Duration: number;
  Key: string;
  LegIndexes: number[];
  Stops: number;
}
