import { FlightInterface } from './flight.interface';
import { LegInterface } from './leg.interface';
import { OfferInterface } from './offer.interface';
import { SegmentInterface } from './segment.interface';

export interface FlightSearchResultInterface {
  Done: boolean;
  ResultNumber: number;
  Flights: FlightInterface[];
  Legs: LegInterface[];
  Offers: OfferInterface[];
  Segments: SegmentInterface[];
}
