export interface FlightTicketSegmentInterface {
  Arrival: string | Date;
  Departure: string | Date;
  Duration: number;
  AirlineName: string;
  AirlineIcao: string;
  OriginIata: string;
  OriginDisplayName: string;
  DestinationIata: string;
  DestinationDisplayName: string;
}
