import { FlightTicketSegmentInterface } from './flight-ticket-segment.interface';

export interface FlightTicketInterface {
  Key: string;
  Segments: FlightTicketSegmentInterface[];
  Price: number;
  Currency: string;
  Deeplink: string;
}
