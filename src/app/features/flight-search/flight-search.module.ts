import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Module Routing --------------------------------------------------------------
import { FlightSearchRoutingModule } from './flight-search-routing.module';
// Components ------------------------------------------------------------------
import { FlightSearchPageComponent } from './pages/flight-search-page/flight-search.component';
import { FlightTicketComponent } from './components/flight-ticket/flight-ticket.component';
import { FlightSegmentComponent } from './components/flight-segment/flight-segment.component';
// Services --------------------------------------------------------------------
import { FlightSearchService } from './services/flight-search-service';
import { FlightSearchHelperService } from './services/flight-search-helper-service';
// Pipes ------------------------------------------------------------------
import { MinutesAsHoursPipe } from './pipes/minutes-as-hours/minutes-as-hours.pipe';

@NgModule({
  declarations: [
    FlightSearchPageComponent,
    FlightTicketComponent,
    FlightSegmentComponent,
    MinutesAsHoursPipe
  ],
  imports: [
    CommonModule,
    FlightSearchRoutingModule,
  ],
  providers: [
    FlightSearchService,
    FlightSearchHelperService
  ]
})
export class FlightSearchModule { }
