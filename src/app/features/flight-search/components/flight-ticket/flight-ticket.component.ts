import { Component, Input, Output, EventEmitter } from '@angular/core';

// Models ----------------------------------------------------------------------
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';

@Component({
  selector: 'flight-ticket',
  templateUrl: './flight-ticket.component.html'
})
export class FlightTicketComponent {
  @Input() public ticket: FlightTicketInterface;
  @Output() public bookButtonClick = new EventEmitter<any>();

  public bookClickHandler(): void {
    this.bookButtonClick.emit();
  }
}
