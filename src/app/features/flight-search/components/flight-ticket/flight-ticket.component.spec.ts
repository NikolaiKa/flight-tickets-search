import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DecimalPipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';

// Components ------------------------------------------------------------------
import { FlightTicketComponent } from './flight-ticket.component';
// Models ----------------------------------------------------------------------
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';
import { FlightTicketSegmentInterface } from '../../models/flight-ticket/flight-ticket-segment.interface';
// Services --------------------------------------------------------------------
import { TestingHelperService } from '../../services/testing-helper-service';

describe('FlightTicketComponent', () => {
  let component: FlightTicketComponent;
  let fixture: ComponentFixture<FlightTicketComponent>;
  let ticketMock: FlightTicketInterface;
  let tester: TestingHelperService;
  let decimalPipe: DecimalPipe;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightTicketComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [DecimalPipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    decimalPipe = TestBed.get(DecimalPipe);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightTicketComponent);
    tester = new TestingHelperService();
    tester.setFixture(fixture);
    component = fixture.componentInstance;
    ticketMock = getTicketMock();
    component.ticket = ticketMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should render price of the ticket', () => {
    const priceSelector = '#flight-segment-component-price';
    const renderedPrice = tester.getTextContentById(priceSelector);

    const mockPriceFormatteByDecimalPipe = decimalPipe.transform(ticketMock.Price, '1.0-3');
    expect(renderedPrice).toContain(mockPriceFormatteByDecimalPipe);
  });

  it('Should render currency of the ticket', () => {
    const priceSelector = '#flight-segment-component-price';
    const renderedPrice = tester.getTextContentById(priceSelector);

    expect(renderedPrice).toContain(ticketMock.Currency);
  });

  it('Click on the Book button should call component\'s "bookClickHandler()" function', () => {
    const bookClickHandlerSpy = spyOn(component, 'bookClickHandler');

    const bookButtonSelector = '#flight-segment-component-book-button';
    const bookButton = tester.getDebugElementById(bookButtonSelector);
    bookButton.triggerEventHandler('click', null);

    expect(bookClickHandlerSpy).toHaveBeenCalled();
  });

  it('Click on the Book button should emit event to the parent component', () => {
    let isEventEmitted = false;
    component.bookButtonClick.subscribe(() => isEventEmitted = true);

    const bookButtonSelector = '#flight-segment-component-book-button';
    const bookButton = tester.getDebugElementById(bookButtonSelector);
    bookButton.triggerEventHandler('click', null);

    expect(isEventEmitted).toBeTruthy();
  });

  // Mocks ---------------------------------------------------------------------
  const getTicketMock = (): FlightTicketInterface => {
    const departureSegment: FlightTicketSegmentInterface = {
      AirlineIcao: 'EZY',
      AirlineName: 'easyJet',
      OriginIata: 'CPH',
      OriginDisplayName: 'Copenhagen',
      Departure: '2018-11-12T13:00:00',
      Duration: 40,
      DestinationIata: 'STN',
      DestinationDisplayName: 'London, Stansted (STN), United Kingdom',
      Arrival: '2018-11-12T13:40:00'
    };

    const arrivalSegment: FlightTicketSegmentInterface = {
      AirlineIcao: 'RYR',
      AirlineName: 'Ryanair',
      OriginIata: 'STN',
      OriginDisplayName: 'London, Stansted (STN), United Kingdom',
      Departure: '2018-11-15T09:00:00',
      Duration: 50,
      DestinationIata: 'CPH',
      DestinationDisplayName: 'Copenhagen',
      Arrival: '2018-11-15T09:50:00'
    };

    const ticket: FlightTicketInterface = {
      Key: 'ECO|SK1264AAR05042035CPH2115,SK1243CPH05050735AAR0815',
      Price: 1200,
      Currency: 'DKK',
      Deeplink: 'http://www.momondo.com',
      Segments: [
        departureSegment,
        arrivalSegment
      ]
    };

    return ticket;
  };

});
