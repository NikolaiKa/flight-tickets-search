import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';

// Components ------------------------------------------------------------------
import { FlightSegmentComponent } from './flight-segment.component';
// Pipes -----------------------------------------------------------------------
import { MinutesAsHoursPipe } from '../../pipes/minutes-as-hours/minutes-as-hours.pipe';
// Models ----------------------------------------------------------------------
import { FlightTicketSegmentInterface } from '../../models/flight-ticket/flight-ticket-segment.interface';
// Services --------------------------------------------------------------------
import { TestingHelperService } from '../../services/testing-helper-service';

describe('FlightSegmentComponent', () => {
  let component: FlightSegmentComponent;
  let fixture: ComponentFixture<FlightSegmentComponent>;
  let tester: TestingHelperService;
  let segmentMock: FlightTicketSegmentInterface;
  let datePipe: DatePipe;
  let durationPipe: MinutesAsHoursPipe;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FlightSegmentComponent,
        MinutesAsHoursPipe
      ],
      providers: [
        DatePipe,
        MinutesAsHoursPipe
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    datePipe = TestBed.get(DatePipe);
    durationPipe = TestBed.get(MinutesAsHoursPipe);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightSegmentComponent);
    tester = new TestingHelperService();
    tester.setFixture(fixture);
    component = fixture.componentInstance;
    segmentMock = getSegmentMock();
    component.segment = segmentMock;
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('Should render airline name', () => {
    const airlineNameSelector = '#flight-segment-component-airline-name';
    const renderedAirlineName = tester.getTextContentById(airlineNameSelector);
    expect(renderedAirlineName).toBe(segmentMock.AirlineName);
  });

  it('Should render origin Iata code', () => {
    const originIataSelector = '#flight-segment-component-origin-iata';
    const renderedOriginIata = tester.getTextContentById(originIataSelector);
    expect(renderedOriginIata).toBe(segmentMock.OriginIata);
  });

  it('Should render formatted departure time', () => {
    const departureTimeSelector = '#flight-segment-component-departure-time';
    const renderedDepartureTime = tester.getTextContentById(departureTimeSelector);

    expect(renderedDepartureTime).toBe(formatDate(segmentMock.Departure));
  });

  it('Should render origin display name', () => {
    const originDisplayNameSelector = '#flight-segment-component-origin-display-name';
    const renderedOriginDisplayName = tester.getTextContentById(originDisplayNameSelector);

    expect(renderedOriginDisplayName).toBe(segmentMock.OriginDisplayName);
  });

  it('Should render segment duration', () => {
    const durationSelector = '#flight-segment-component-duration';
    const renderedDuration = tester.getTextContentById(durationSelector);

    expect(renderedDuration).toBe(formatDuration(segmentMock.Duration));
  });

  it('Should render destination iata code', () => {
    const destinationIataSelector = '#flight-segment-component-destination-iata';
    const renderedDestinationIata = tester.getTextContentById(destinationIataSelector);

    expect(renderedDestinationIata).toBe(segmentMock.DestinationIata);
  });

  it('Should render formatted arrival time', () => {
    const arrivalTimeSelector = '#flight-segment-component-arrival-time';
    const renderedArrivalTime = tester.getTextContentById(arrivalTimeSelector);

    expect(renderedArrivalTime).toBe(formatDate(segmentMock.Arrival));
  });

  it('Should render destination display name', () => {
    const destinationDisplayNameSelector = '#flight-segment-component-destination-display-name';
    const renderedDestinationDisplayName = tester.getTextContentById(destinationDisplayNameSelector);

    expect(renderedDestinationDisplayName).toBe(segmentMock.DestinationDisplayName);
  });

  // Mocks ---------------------------------------------------------------------
  const getSegmentMock = (): FlightTicketSegmentInterface => {
    return {
      AirlineIcao: 'EZY',
      AirlineName: 'easyJet',
      OriginIata: 'CPH',
      OriginDisplayName: 'Copenhagen',
      Departure: '2018-11-12T13:00:00',
      Duration: 40,
      DestinationIata: 'STN',
      DestinationDisplayName: 'London, Stansted (STN), United Kingdom',
      Arrival: '2018-11-12T13:40:00'
    };
  };

  // Helpers -------------------------------------------------------------------
  function formatDate(timestamp: string | Date): string {
    return datePipe.transform(new Date(timestamp), 'HH:mm');
  }

  function formatDuration(minutes: number): string {
    return durationPipe.transform(minutes);
  }
});
