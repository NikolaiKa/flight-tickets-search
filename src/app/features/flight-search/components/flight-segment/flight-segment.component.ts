import { Component, Input } from '@angular/core';

// Models ----------------------------------------------------------------------
import { FlightTicketSegmentInterface } from '../../models/flight-ticket/flight-ticket-segment.interface';

@Component({
  selector: 'flight-segment',
  templateUrl: './flight-segment.component.html'
})
export class FlightSegmentComponent {
  @Input() public segment: FlightTicketSegmentInterface;
}
