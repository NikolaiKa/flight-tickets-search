import { TestBed, inject } from '@angular/core/testing';

// Services --------------------------------------------------------------------
import { FlightSearchHelperService } from './flight-search-helper.service';
// Models ----------------------------------------------------------------------
import { FlightInterface } from '../../models/flight/flight.interface';
import { FlightSearchResultInterface } from '../../models/flight/flight-search-result.interface';
import { SegmentInterface } from '../../models/flight/segment.interface';
import { OfferInterface } from '../../models/flight/offer.interface';
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';

describe('FlightSearchHelperService', () => {
  let service: FlightSearchHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FlightSearchHelperService
      ]
    });
  });

  beforeEach(inject([FlightSearchHelperService], s => (service = s)));

  it('Should create service', () => {
    expect(service).toBeDefined();
  });

  // getFlightByIndex()
  it('Should extract flight at specified index form flights array', () => {
    const { Flights } = flightsSearchResultsMock();

    const secondFlightIndex = 1;
    const secondFlight = service['getFlightByIndex'](Flights, secondFlightIndex);

    expect(secondFlight).toEqual(Flights[secondFlightIndex]);
  });

  // getFlightSegments()
  it('Should find and return segments related to the flight', () => {
    const { Segments} = flightsSearchResultsMock();

    const testFlight = { SegmentIndexes: [0, 3]} as FlightInterface;
    const flightSegments = service['getFlightSegments'](testFlight, Segments);

    expect(flightSegments[0]).toEqual(Segments[0]);
    expect(flightSegments[1]).toEqual(Segments[3]);
  });

  // concatSegmentsLegsIndexes()
  it('Should concat leg indexes of segments', () => {
    const segments = [
      {
        LegIndexes: [0]
      },
      {
        LegIndexes: [2]
      },
    ] as SegmentInterface[];

    const legIndexes = service['concatSegmentsLegsIndexes'](segments);

    expect(legIndexes).toEqual([0, 2]);
  });

  // getLegsByIndexes()
  it('Should return legs at requested indexes', () => {
    const { Legs } = flightsSearchResultsMock();
    const legIndexes = [1, 4];

    const desiredLegs = service['getLegsByIndexes'](Legs, legIndexes);

    expect(desiredLegs[0]).toEqual(Legs[1]);
    expect(desiredLegs[1]).toEqual(Legs[4]);
  });

  // mapLegFlightTicketSegmentInterface()
  it('Should map LegInterface to FlightTicketSegmentInterface', () => {
    const { Legs } = flightsSearchResultsMock();
    const [ firstLeg ] = Legs;

    const result = service['mapLegFlightTicketSegmentInterface'](firstLeg);

    expect(result).toEqual({
      Arrival: '2016-05-04T21:15:00',
      Departure: '2016-05-04T20:35:00',
      Duration: 40,
      AirlineIcao: 'SAS',
      AirlineName: 'SAS Scandinavian Airlines',
      OriginIata: 'AAR',
      OriginDisplayName: 'Aarhus (AAR), Denmark',
      DestinationIata: 'CPH',
      DestinationDisplayName: 'Copenhagen (CPH), Denmark'
    });
  });

  // makeFlightSearchRequestUrl()
  it('Should make flight search request url', () => {
    const proxyUrl = 'https://cors-proxy.com/';
    const apiBaseUrl = 'http://momondodevapi.com/';
    const requestParams = 'flight-search/CPH-AAR/2018-11-29';

    const constructedUrl = service['makeFlightSearchRequestUrl'](apiBaseUrl, requestParams, proxyUrl);

    expect(constructedUrl).toBe(`${proxyUrl}${apiBaseUrl}${requestParams}`);
  });

  // makeFlightSearchRequestUrl()
  it('Should not prepend request url with proxy if it is not specified', () => {
    const apiBaseUrl = 'http://momondodevapi.com/';
    const requestParams = 'flight-search/CPH-AAR/2018-11-29';

    const constructedUrl = service['makeFlightSearchRequestUrl'](apiBaseUrl, requestParams);

    expect(constructedUrl).toBe(`${apiBaseUrl}${requestParams}`);
  });

  // concatArrays()
  it('Should concatenate contents of 2 arrays', () => {
    const flights1 = [{Key: 'flights1Key'}] as FlightInterface[];
    const flights2 = [{Key: 'flights2Key'}] as FlightInterface[];

    const result = service['concatArrays'](flights1, flights2);

    expect(result).toEqual([...flights1, ...flights2]);
  });

  // sortOffersByPrice()
  it('Should sort offers by price', () => {
    const offers = [
      {Price: 1300},
      {Price: 1100},
      {Price: 1500},
      {Price: 1200},
      {Price: 1400},
    ] as OfferInterface[];

    const sortedOffers = service['sortOffersByPrice'](offers);

    expect(sortedOffers).toEqual([
      {Price: 1100},
      {Price: 1200},
      {Price: 1300},
      {Price: 1400},
      {Price: 1500}
    ] as OfferInterface[]);
  });

  // concatSearchResults()
  it('When handling 1-st search result should simply return it without modifying', () => {
    const searchResult = {
      ResultNumber: 0, // First search result
      Done: false,
      Flights: [],
      Offers: [],
      Segments: [],
      Legs: []
    } as FlightSearchResultInterface;

    const contatenated = service.concatSearchResults(undefined, searchResult);

    expect(contatenated).toEqual(searchResult);
  });

  // concatSearchResults()
  it('Should concatenate flight search results', () => {
    const searchResult1 = {
      Flights: [
        {
          Key: '12345',
          SegmentIndexes: [0, 1],
          TicketClass: 'ECO'
        }
      ],
      Offers: [
        {
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 0,
          TicketClass: 'DKK',
          Price: 1200
        }
      ],
      Segments: [
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2114',
          LegIndexes: [0]
        },
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2115',
          LegIndexes: [1]
        }
      ],
      Legs: [
        {
          OriginIata: 'TLL',
          DestinationIata: 'CPH',
          AirlineName: 'SAS'
        },
        {
          OriginIata: 'CPH',
          DestinationIata: 'TLL',
          AirlineName: 'SAS'
        }
      ]
    } as FlightSearchResultInterface;

    const searchResult2 = {
      Flights: [
        {
          Key: '12345',
          SegmentIndexes: [2, 3],
          TicketClass: 'ECO'
        }
      ],
      Offers: [
        {
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 1,
          TicketClass: 'DKK',
          Price: 1500
        }
      ],
      Segments: [
        {
          Duration: 45,
          Key: 'SK1264AAR05042035CPH2116',
          LegIndexes: [2]
        },
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2117',
          LegIndexes: [3]
        }
      ],
      Legs: [
        {
          OriginIata: 'TLL',
          DestinationIata: 'CPH',
          AirlineName: 'LOT'
        },
        {
          OriginIata: 'CPH',
          DestinationIata: 'TLL',
          AirlineName: 'LOT'
        }
      ]
    } as FlightSearchResultInterface;

    const concatenated = service.concatSearchResults(searchResult1, searchResult2);

    expect(concatenated).toEqual({
      Flights: [
        {
          Key: '12345',
          SegmentIndexes: [0, 1],
          TicketClass: 'ECO'
        },
        {
          Key: '12345',
          SegmentIndexes: [2, 3],
          TicketClass: 'ECO'
        }
      ],
      Offers: [
        {
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 0,
          TicketClass: 'DKK',
          Price: 1200
        },
        {
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 1,
          TicketClass: 'DKK',
          Price: 1500
        }
      ],
      Segments: [
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2114',
          LegIndexes: [0]
        },
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2115',
          LegIndexes: [1]
        },
        {
          Duration: 45,
          Key: 'SK1264AAR05042035CPH2116',
          LegIndexes: [2]
        },
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2117',
          LegIndexes: [3]
        }
      ],
      Legs: [
        {
          OriginIata: 'TLL',
          DestinationIata: 'CPH',
          AirlineName: 'SAS'
        },
        {
          OriginIata: 'CPH',
          DestinationIata: 'TLL',
          AirlineName: 'SAS'
        },
        {
          OriginIata: 'TLL',
          DestinationIata: 'CPH',
          AirlineName: 'LOT'
        },
        {
          OriginIata: 'CPH',
          DestinationIata: 'TLL',
          AirlineName: 'LOT'
        }
      ]
    } as FlightSearchResultInterface);
  });

  // convertOfferToFlightTicket()
  it('Should extract and combine information related to a specific offer', () => {
    const searchResults = flightsSearchResultsMock();
    const firstOffer = searchResults.Offers[0];

    const flightTicket = service['convertOfferToFlightTicket'](firstOffer, searchResults);

    expect(flightTicket).toEqual({
      Price: 1200,
      Currency: 'HUF',
      Deeplink: 'http://www.momondo.com',
      Key: 'ECO|SK1264AAR05042035CPH2115,SK1243CPH05050735AAR0815',
      Segments: [
        {
          Arrival: '2016-05-04T21:15:00',
          Departure: '2016-05-04T20:35:00',
          Duration: 40,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        },
        {
          Arrival: '2016-05-05T08:15:00',
          Departure: '2016-05-05T07:35:00',
          Duration: 40,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'CPH',
          OriginDisplayName: 'Copenhagen (CPH), Denmark',
          DestinationIata: 'AAR',
          DestinationDisplayName: 'Aarhus (AAR), Denmark'
        },
      ]
    } as FlightTicketInterface);
  });

  // removeDuplicatedFlightOffers()
  it('Should remove duplicate flight offers', () => {
    const offers = [
      {Price: 1100, FlightIndex: 0},
      {Price: 1200, FlightIndex: 0},
      {Price: 1300, FlightIndex: 1},
      {Price: 1400, FlightIndex: 1},
      {Price: 1500, FlightIndex: 2}
    ] as OfferInterface[];

    const uniqueOffers = service['removeDuplicatedFlightOffers'](offers);

    expect(uniqueOffers).toEqual([
      {Price: 1100, FlightIndex: 0},
      {Price: 1300, FlightIndex: 1},
      {Price: 1500, FlightIndex: 2}
    ] as OfferInterface[]);
  });

  // Mocks ---------------------------------------------------------------------
  const flightsSearchResultsMock = (): FlightSearchResultInterface => {
    return {
      Done: false,
      ResultNumber: 0,
      Flights: [
        {
          Key: 'ECO|SK1264AAR05042035CPH2115,SK1243CPH05050735AAR0815',
          MaxStops: 0,
          SegmentIndexes: [
            0,
            1
          ],
          TicketClass: 'ECO'
        },
        {
          Key: 'ECO|SK1264AAR05042035CPH2115,SK1247CPH05051230AAR1310',
          MaxStops: 0,
          SegmentIndexes: [
            0,
            2
          ],
          TicketClass: 'ECO'
        },
        {
          Key: 'ECO|SK1240AAR05040635CPH0715,SK1243CPH05050735AAR0815',
          MaxStops: 0,
          SegmentIndexes: [
            3,
            1
          ],
          TicketClass: 'ECO'
        }
      ],
      Legs: [
        {
          Arrival: '2016-05-04T21:15:00',
          Departure: '2016-05-04T20:35:00',
          Duration: 40,
          FlightNumber: 1264,
          Key: 'SK1264AAR05042035CPH2115',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        },
        {
          Arrival: '2016-05-05T08:15:00',
          Departure: '2016-05-05T07:35:00',
          Duration: 40,
          FlightNumber: 1243,
          Key: 'SK1243CPH05050735AAR0815',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'CPH',
          OriginDisplayName: 'Copenhagen (CPH), Denmark',
          DestinationIata: 'AAR',
          DestinationDisplayName: 'Aarhus (AAR), Denmark'
        },
        {
          Arrival: '2016-05-05T13:10:00',
          Departure: '2016-05-05T12:30:00',
          Duration: 40,
          FlightNumber: 1247,
          Key: 'SK1247CPH05051230AAR1310',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'CPH',
          OriginDisplayName: 'Copenhagen (CPH), Denmark',
          DestinationIata: 'AAR',
          DestinationDisplayName: 'Aarhus (AAR), Denmark'
        },
        {
          Arrival: '2016-05-04T07:15:00',
          Departure: '2016-05-04T06:35:00',
          Duration: 40,
          FlightNumber: 1240,
          Key: 'SK1240AAR05040635CPH0715',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        },
        {
          Arrival: '2016-05-04T11:30:00',
          Departure: '2016-05-04T10:50:00',
          Duration: 40,
          FlightNumber: 1246,
          Key: 'SK1246AAR05041050CPH1130',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        },
        {
          Arrival: '2016-05-04T14:15:00',
          Departure: '2016-05-04T13:35:00',
          Duration: 40,
          FlightNumber: 1248,
          Key: 'SK1248AAR05041335CPH1415',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        },
        {
          Arrival: '2016-05-04T19:00:00',
          Departure: '2016-05-04T18:20:00',
          Duration: 40,
          FlightNumber: 1260,
          Key: 'SK1260AAR05041820CPH1900',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        },
        {
          Arrival: '2016-05-05T15:25:00',
          Departure: '2016-05-05T14:45:00',
          Duration: 40,
          FlightNumber: 1257,
          Key: 'SK1257CPH05051445AAR1525',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'CPH',
          OriginDisplayName: 'Copenhagen (CPH), Denmark',
          DestinationIata: 'AAR',
          DestinationDisplayName: 'Aarhus (AAR), Denmark'
        },
        {
          Arrival: '2016-05-04T09:20:00',
          Departure: '2016-05-04T08:40:00',
          Duration: 40,
          FlightNumber: 1244,
          Key: 'SK1244AAR05040840CPH0920',
          StopOvers: 0,
          AirlineIcao: 'SAS',
          AirlineName: 'SAS Scandinavian Airlines',
          OriginIata: 'AAR',
          OriginDisplayName: 'Aarhus (AAR), Denmark',
          DestinationIata: 'CPH',
          DestinationDisplayName: 'Copenhagen (CPH), Denmark'
        }
      ],
      Offers: [
        {
          Currency: 'HUF',
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 0,
          Score: 175.60,
          TicketClass: 'ECO',
          Price: 1200
        },
        {
          Currency: 'HUF',
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 1,
          Score: 185.35,
          TicketClass: 'ECO',
          Price: 1300
        },
        {
          Currency: 'HUF',
          Deeplink: 'http://www.momondo.com',
          FlightIndex: 2,
          Score: 195.20,
          TicketClass: 'ECO',
          Price: 1400
        },
      ],
      Segments: [
        {
          Duration: 40,
          Key: 'SK1264AAR05042035CPH2115',
          LegIndexes: [
            0
          ],
          Stops: 0
        },
        {
          Duration: 40,
          Key: 'SK1243CPH05050735AAR0815',
          LegIndexes: [
            1
          ],
          Stops: 0
        },
        {
          Duration: 40,
          Key: 'SK1247CPH05051230AAR1310',
          LegIndexes: [
            2
          ],
          Stops: 0
        },
        {
          Duration: 40,
          Key: 'SK1240AAR05040635CPH0715',
          LegIndexes: [
            3
          ],
          Stops: 0
        }
      ]
    };
  };
});
