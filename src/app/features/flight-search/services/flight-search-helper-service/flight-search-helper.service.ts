import { Injectable } from '@angular/core';

// lodash
import * as _ from 'lodash';

// Models ----------------------------------------------------------------------
import { FlightInterface } from '../../models/flight/flight.interface';
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';
import { FlightTicketSegmentInterface } from '../../models/flight-ticket/flight-ticket-segment.interface';
import { FlightSearchResultInterface } from '../../models/flight/flight-search-result.interface';
import { LegInterface } from '../../models/flight/leg.interface';
import { OfferInterface } from '../../models/flight/offer.interface';
import { SegmentInterface } from '../../models/flight/segment.interface';
@Injectable()
export class FlightSearchHelperService {

  /*
    Processes flight search results.
    Returns array of FlightTicketInterface type objects
  */
  public convertSearchResultsToFlightTickets(searchResults: FlightSearchResultInterface): FlightTicketInterface[] {
    const {Offers} = searchResults;

    const offersSortedByPrice = this.sortOffersByPrice(Offers);
    // Removing duplicated flight offers. Keeping only 1 cheapest offer per flight
    const uniqFlightOffers = this.removeDuplicatedFlightOffers(offersSortedByPrice);

    const flightTickets = uniqFlightOffers.map(offer => this.convertOfferToFlightTicket(offer, searchResults));

    return flightTickets;
  }

  /*
    Returns flight at specific index in the flights array
  */
  public getFlightByIndex(flights: FlightInterface[], index: number): FlightInterface {
    return flights[index];
  }

  /*
    Finds and returns segments related to the flight
    segments: [{segment1}, {segment2}, {segment3}, {segment4}]
    flight: { SegmentIndexes: [0,2] }

    result = getFlightSegments(flight, segments)
    result => [{segment1}, {segment3}]
  */
  public getFlightSegments(flight: FlightInterface, segments: SegmentInterface[]): SegmentInterface[] {
    return flight.SegmentIndexes.map(segmentIndex => segments[segmentIndex]);
  }

  /*
    Contacts leg indexes of the segments
    Example:
    segment1 = { LegIndexes: [0] }
    segment2 = { LegIndexes: [2] }

    legIndexes = concatSegmentsLegsIndexes([segment1,segment2])
    legIndexes = > [0, 2]
  */
  public concatSegmentsLegsIndexes(segments: SegmentInterface[]): number[] {
    let legIndexes = [];
    segments.forEach(segment => {
      legIndexes = legIndexes.concat(segment.LegIndexes);
    });

    return legIndexes;
  }

  /*
    Returns legs on requested indexes
    Example:
    legs = [ {leg1}, {leg2}, {leg3}, {leg4}]
    legIndexes = [0, 3]

    result = getLegsByIndexes(legs, legIndexes)
    result = > [{leg1}, {leg4}]
  */
  public getLegsByIndexes(legs: LegInterface[], legsIndexes: number[]): LegInterface[] {
    return legsIndexes.map(legIndex => legs[legIndex]);
  }

  /*
  Combines information of flight offer and related legs of the flight
  */
  public creteFlightTicket(flightOffer: OfferInterface, flightLegs: LegInterface[] , flightKey: string): FlightTicketInterface {
    return {
      Key: flightKey,
      Price: flightOffer.Price,
      Currency: flightOffer.Currency,
      Deeplink: flightOffer.Deeplink,
      Segments: flightLegs.map(leg => this.mapLegFlightTicketSegmentInterface(leg))
    } as FlightTicketInterface;
  }

  public makeFlightSearchRequestUrl(apiBaseUrl: string, requestParams: string, corsProxy: string = ''): string {
    return `${corsProxy}${apiBaseUrl}${requestParams}`;
  }

  /*
    Concatenates arrays of the flight search results
    result1 = { flights: [ {flight1}, {flight2} ] ....}
    result2 = { flights: [ {flight3}, {flight4} ] ....}

    result = concatSearchResults(result1, result2)
    result = { flights: [ {flight1}, {flight2}, {flight3}, {flight4} ] ....}
  */
  public concatSearchResults(
    searchResult1: FlightSearchResultInterface, searchResult2: FlightSearchResultInterface
  ): FlightSearchResultInterface {

    const isFirstSearchResult = searchResult2.ResultNumber === 0;
    if (isFirstSearchResult) { return searchResult2; }

    return {
      Flights: this.concatArrays(searchResult1.Flights, searchResult2.Flights),
      Offers: this.concatArrays(searchResult1.Offers, searchResult2.Offers),
      Legs: this.concatArrays(searchResult1.Legs, searchResult2.Legs),
      Segments: this.concatArrays(searchResult1.Segments, searchResult2.Segments)
    } as FlightSearchResultInterface;
  }

  /*
    Sorting offers by Price (ascending)

    Example:
    offers = [{price: 1500}, {price: 1200}, {price: 1300}]
    sortedOffers = sortOffersByPrice(offers)
    uniqueOffers = [{price: 1200}, {price: 1300}, {price: 1500}]
  */
  public sortOffersByPrice(offers: OfferInterface[]): OfferInterface[] {
   return _.sortBy(offers, (offer) => offer.Price);
  }

  private concatArrays(array1: Array<any>, array2: Array<any>): Array<any> {
    return [...array1, ...array2];
  }

  /*
    Maps LegInterface to FlightTicketSegmentInterface
  */
  private mapLegFlightTicketSegmentInterface(leg: LegInterface): FlightTicketSegmentInterface {
    return {
      AirlineIcao: leg.AirlineIcao,
      AirlineName: leg.AirlineName,
      Arrival: leg.Arrival,
      Departure: leg.Departure,
      DestinationDisplayName: leg.DestinationDisplayName,
      DestinationIata: leg.DestinationIata,
      Duration: leg.Duration,
      OriginDisplayName: leg.OriginDisplayName,
      OriginIata: leg.OriginIata
    };
  }

  /*
    Extracts from search results information related to a specific flight offer.
    Extracted and combined information is mapped into FlightTicketInterface
  */
  private convertOfferToFlightTicket(offer: OfferInterface, searchResults: FlightSearchResultInterface): FlightTicketInterface {
    const { Flights, Segments, Legs } = searchResults;

    const offerFlight = this.getFlightByIndex(Flights, offer.FlightIndex);
    const offerSegments = this.getFlightSegments(offerFlight, Segments);
    const offerLegIndexes = this.concatSegmentsLegsIndexes(offerSegments);
    const offerLegs = this.getLegsByIndexes(Legs, offerLegIndexes);
    const ticket = this.creteFlightTicket(offer, offerLegs, offerFlight.Key);

    return ticket;
  }

  /*
    Removes duplicate flight offers -> offers with the same FlightIndex
    Leaves only 1 offer per flight.

    Example:
    offers = [{FlightIndex: 0}, {FlightIndex: 0}, {FlightIndex: 1}]
    uniqueOffers = removeDuplicatedFlightOffers(offers)
    uniqueOffers =  [{FlightIndex: 0}, {FlightIndex: 1}]
  */
  private removeDuplicatedFlightOffers(offers: OfferInterface[]): OfferInterface[] {
    return _.uniqBy(offers, 'FlightIndex');
  }
}
