import { DebugElement } from '@angular/core';
import { ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

export class TestingHelperService {
  private fixture: ComponentFixture<any>;

  constructor() {}

  public setFixture(fixture: ComponentFixture<any>): void {
    this.fixture = fixture;
  }

  /*
    Finds element with the specific id and return it's text value as a String
  */
  public getTextContentById(cssIdSelector: string): string {
    const debugElement = this.fixture.debugElement.query(By.css(cssIdSelector));

    return this.getTextContentOfDebugElement(debugElement);
  }

  /*
    Finds all elements with the given class and return their text values as an Array
  */
  public getTextContentByClass(cssClassSelector: string): string[] {
    const debugElements = this.fixture.debugElement.queryAll(By.css(cssClassSelector));
    return debugElements.map(de => this.getTextContentOfDebugElement(de));
  }

   /*
    Return trimmed text value of a DebugElement
  */
  public getTextContentOfDebugElement(debugElement: DebugElement): string {
    const nativeElement: HTMLElement = debugElement.nativeElement;
    return nativeElement.textContent.trim();
  }

  public getDebugElementById(cssIdSelector: string): DebugElement | null {
    return this.fixture.debugElement.query(By.css(cssIdSelector));
  }

  public getDebugElementsByClass(cssClassSelector: string): DebugElement[] | null {
    return this.fixture.debugElement.queryAll(By.css(cssClassSelector));
  }
}
