import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Rxjs ------------------------------------------------------------------------
import { Subject, Observable } from 'rxjs';
// Models ----------------------------------------------------------------------
import { FlightSearchResultInterface } from '../../models/flight/flight-search-result.interface';
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';
// Services --------------------------------------------------------------------
import { FlightSearchHelperService } from '../flight-search-helper-service';

@Injectable()
export class FlightSearchService {
  private readonly corsProxy = 'https://cors-anywhere.herokuapp.com/';
  private readonly apiBaseUrl = 'http://momondodevapi.herokuapp.com/api/1.0/';

  private flightTickets$: Subject<FlightTicketInterface[]> = new Subject();
  private flightSearchResults: FlightSearchResultInterface;

  constructor(
    private http: HttpClient,
    private helper: FlightSearchHelperService
    ) { }

  public getFlightTickets(): Observable<FlightTicketInterface[]> {
    return this.flightTickets$.asObservable();
  }

  public searchFlights(uuid: string): void {
    const requestUrl = this.helper.makeFlightSearchRequestUrl(this.apiBaseUrl, `FlightSearch/${uuid}`, this.corsProxy);

    this.http.get<FlightSearchResultInterface>(requestUrl).subscribe(searchResults => {
      this.processSearchResults(searchResults, uuid);
    });
  }

  private processSearchResults(searchResult: FlightSearchResultInterface, uuid: string): void {
    // If search is not done yet, send another search request using UUID of the current search
    if (!searchResult.Done) { this.searchFlights(uuid); }

    // Concatenate current search results with existing search results
    this.flightSearchResults = this.helper.concatSearchResults(this.flightSearchResults, searchResult);

    // Convert search results into flight tickets
    const flightTickets = this.helper.convertSearchResultsToFlightTickets(this.flightSearchResults);

    // Emit flight tickets
    this.flightTickets$.next(flightTickets);
  }
}
