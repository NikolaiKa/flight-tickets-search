import { TestBed, inject } from '@angular/core/testing';
import { FlightSearchService } from './flight-search.service';
import { HttpClient } from '@angular/common/http';

// Services --------------------------------------------------------------------
import { FlightSearchHelperService } from '../flight-search-helper-service';
// Models ----------------------------------------------------------------------
import { FlightSearchResultInterface } from '../../models/flight/flight-search-result.interface';
import { FlightTicketInterface } from '../../models/flight-ticket/flight-ticket.interface';

describe('FlightSearchService', () => {
  let service: FlightSearchService;
  let httpMock: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {

    setupMocks();

    TestBed.configureTestingModule({
      providers: [
        FlightSearchService,
        FlightSearchHelperService,
        { provide: HttpClient, useValue: httpMock }
      ]
    });
  });

  beforeEach(inject([FlightSearchService], s => (service = s)));

  it('Should create service', () => {
    expect(service).toBeDefined();
  });

  // processSearchResults()
  it('"processSearchResults()" should emit flightTickets', () => {
    mockConvertSearchResultsToFlightTicketsFunction();
    let receivedFlightTickets;
    service.getFlightTickets().subscribe(data => receivedFlightTickets = data);

    const resultMock = { Done: true, ResultNumber: 0 } as FlightSearchResultInterface;
    service['processSearchResults'](resultMock, 'uuid');

    expect(receivedFlightTickets).toBeDefined();
    expect(receivedFlightTickets).toEqual(flightTicketsMock());
  });

  // processSearchResults()
  it('"processSearchResults()" should call "searchFlights()" if search is not done yet', () => {
    mockConvertSearchResultsToFlightTicketsFunction();
    const searchFlightsSpy = spyOn(service, 'searchFlights').and.callFake(() => {});

    const resultMock = { Done: false, ResultNumber: 0 } as FlightSearchResultInterface;
    const uuidMock = '01ea4b85-72c5-4751-95bb-72af335293f1';
    service['processSearchResults'](resultMock, uuidMock);

    expect(searchFlightsSpy).toHaveBeenCalledWith(uuidMock);
  });

  // processSearchResults()
  it('"processSearchResults()" should NOT call "searchFlights()" when search is completed', () => {
    mockConvertSearchResultsToFlightTicketsFunction();
    const searchFlightsSpy = spyOn(service, 'searchFlights').and.callFake(() => {});

    const resultMock = { Done: true, ResultNumber: 0 } as FlightSearchResultInterface;
    service['processSearchResults'](resultMock, 'uuidMock');

    expect(searchFlightsSpy).not.toHaveBeenCalled();
  });

  // Helpers -------------------------------------------------------------------
  const setupMocks = (): void => {
    httpMock = jasmine.createSpyObj('HttpClient', ['get']);
  };

  const mockConvertSearchResultsToFlightTicketsFunction = () => {
    const flightSearchHelperService = TestBed.get(FlightSearchHelperService);
    spyOn(flightSearchHelperService, 'convertSearchResultsToFlightTickets').and.callFake(() => {
      return flightTicketsMock();
    });
  };

  // Mocks ---------------------------------------------------------------------
  const flightTicketsMock = (): FlightTicketInterface[] => {
    return [
      {
        Key: 'ECO|SK1264AAR05042035CPH2115,SK1243CPH05050735AAR0815',
        Price: 1200,
        Currency: 'DKK',
        Deeplink: 'http://www.momondo.com',
        Segments: [
          {
            AirlineIcao: 'EZY',
            AirlineName: 'easyJet',
            OriginIata: 'CPH',
            OriginDisplayName: 'Copenhagen',
            Departure: '2018-11-12T13:00:00',
            Duration: 40,
            DestinationIata: 'STN',
            DestinationDisplayName: 'London, Stansted (STN), United Kingdom',
            Arrival: '2018-11-12T13:40:00'
          },
          {
            AirlineIcao: 'RYR',
            AirlineName: 'Ryanair',
            OriginIata: 'STN',
            OriginDisplayName: 'London, Stansted (STN), United Kingdom',
            Departure: '2018-11-15T09:00:00',
            Duration: 50,
            DestinationIata: 'CPH',
            DestinationDisplayName: 'Copenhagen',
            Arrival: '2018-11-15T09:50:00'
          }
        ]
      }
    ];
  };
});
