import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'flight-search',
    pathMatch: 'full'
  },
  {
    path: 'flight-search',
    loadChildren: './features/flight-search/flight-search.module#FlightSearchModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
